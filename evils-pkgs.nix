{ config, lib, pkgs, ...}: {

  environment.systemPackages = with pkgs; [
    # system utilities
    bc jq gcc git zsh tree
    rsync unzip btrfs-progs
    rxvt_unicode oh-my-zsh imagemagick noto-fonts
    edac-utils usbutils pciutils moreutils lshw lsof
    wget
    # informational
    arp-scan htop lm_sensors smartmontools pv dmidecode hddtemp
    linuxPackages.tp_smapi tpacpi-bat
    # comforts
    vim beep vlc firefox neofetch speedtest-cli stress thefuck numlockx
    clarissa clar
    #platformio
  ];

  programs = {
    vim.defaultEditor = true;
#    sway.enable = true;
    gnupg.agent.enable = true;
  };

}
