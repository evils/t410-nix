{ config, lib, pkgs, ...}: {

  programs.zsh = {
    enable = true;
    autosuggestions = {
      enable = true;
      strategy = [ "match_prev_cmd" ];
    };
    enableCompletion = true;
    syntaxHighlighting = {
      enable = true;
    };
    shellAliases = {
      "btrfs" = "sudo btrfs";
      "docker" = "sudo docker";
      "weather" = "curl -Ss \"https://wttr.in/?0&Q\"";
    };
    interactiveShellInit = ''
        ZSH_DISABLE_COMPFIX=true
        export ZSH=$HOME/.oh-my-zsh
        source $ZSH/oh-my-zsh.sh
        ZSH_CUSTOM=$ZSH/custom/
    '';
   promptInit = "";
  };

#  programs.zsh.ohMyZsh = {
#    enable = true;
#    plugins = [
#        "git"
#        "history-substring-search"
#        "thefuck"
#        "vi-mode"
#    ];
#    #no such file or directory (can't find custom theme)
#    #theme = "evils";
#
#    # uncomment in case of fuckup
#    theme = "terminalparty";
#  };

}
