{ config, lib, pkgs, ...}: {

  imports = [ <musnix> ];
  musnix.enable = true;
  users.users.evils.extraGroups = [ "audio" ];

  environment.systemPackages = with pkgs; [
    pavucontrol
  ];

  sound.enable = true;
  nixpkgs.config.pulseaudio = true;

  # some of the suggested settings for real time audio from the nixos wiki page on pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  environment.etc = let
    json = pkgs.formats.json {};
  in {
    "pipewire/pipewire.d/92-low-latency.conf".source = json.generate "92-low-latency.conf" {
      context.properties = {
        default.clock.rate = 192000;
        default.clock.quantum = 512;
        default.clock.min-quantum = 32;
        default.clock.max-quantum = 512;
      };
    };
    "pipewire/jack.conf.d/jack.conf".source = json.generate "jack.conf" {
      jack.properties.node.latency = "512/192000";
    };
  };

}
