{ config, lib, pkgs, ...}: {

  services.xserver = {
    enable = true;
    layout = "us,us";
    xkbVariant = "dvorak,";
    xkbOptions = "grp:shifts_toggle, lv3:ralt_switch, compose:ralt";
    libinput.enable = true;
    libinput.touchpad.disableWhileTyping = true;
    synaptics.palmDetect = true;

    displayManager.defaultSession = "none+i3";

#    displayManager.startx.enable = true;

    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dmenu
        i3status
        i3blocks
        i3lock
        xclip
      ];
    };

  videoDrivers = [ "mesa" "vulkan" ];

  };
}
