# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      <nixos-hardware/lenovo/thinkpad/t410>

      ./hardware-configuration.nix
      ./evils-pkgs.nix
      ./evils-i3.nix
      ./evils-zsh_oh-my-zsh.nix
      ./audio.nix
    ];

  services.tuptime.enable = true;

  services.clarissa = {
	enable = true;
	nags = 6;
	timeout = 10000;
  };

  # attempt to fix coreboot caused lockup issue, from https://ticket.coreboot.org/issues/121
#  boot.kernelParams = [ "intel_idle.max_cstate=0" "processor.max_cstate=1" ];
  # from https://gist.github.com/rdesfo/6147658
  boot.blacklistedKernelModules = [ "intel_ips" ];
  # not found...
  # boot.initrd.kernelModules = [ "i915/nintel_ips" ];

  hardware.opengl = {
    enable = true;
    extraPackages = [ pkgs.vaapiIntel ];
  };
  hardware.sane.enable = true;
  #hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];
  #nixpkgs.config.allowUnfree = true;
  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez;
  };
  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
  };

  boot.kernelPackages = pkgs.linuxKernel.packages.linux_5_15;
  boot.zfs.requestEncryptionCredentials = true;
  boot.supportedFilesystems = [ "zfs" ];

  boot.loader.timeout = 2;
  boot.loader.grub.memtest86.enable = true;
  boot.loader.grub.copyKernels = true;

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/disk/by-id/ata-Samsung_SSD_850_EVO_500GB_S3R3NB0JB02211F"; # or "nodev" for efi only

  networking = {
    hostName = "evils-nix"; # Define your hostname.
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    hostId = "d3139588";
    networkmanager.enable = true;
    usePredictableInterfaceNames = true;
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  console.font = "Lat2-Terminus16";
  console.keyMap = "dvorak";

  # Set your time zone.
  time.timeZone = "Europe/Brussels";


  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings.PasswordAuthentication = false;

  services.logind.lidSwitch = "ignore";
  services.smartd.enable = true;

  # this is turning the fan off even at 105°C!
  services.thinkfan.enable = false;

  services.fwupd.enable = true;

  # maybe upstream this to nixos-hardware t410?
  # from https://github.com/NixOS/nixos-hardware/pull/154/files
  # modified...
  services.tlp = {
    enable = true;
    settings = {
      START_CHARGE_THRESH_BAT0 = 65;
      STOP_CHARGE_THRESH_BAT0 = 75;
      # https://wiki.archlinux.org/index.php/TLP#Btrfs
      SATA_LINKPWR_ON_BAT = "max_performance";
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [ hplip ];

  services.udev = {
    packages = with pkgs; [
      stlink
      #platformio
      qmk-udev-rules
    ];
  };

  # for liquidctl (NZXT e500)
  services.udev.extraRules = ''
    SUBSYSTEM=="hidraw", SUBSYSTEMS=="usb", ATTRS{idVendor}=="7793", ATTRS{idProduct}=="5911", MODE="0666"
  '';

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.evils = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "audio" "dialout" "scanner" ]; # Enable ‘sudo’ for the user.
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBj6N5zlcXIg5RzmTFovzGU3a80LvXUmnkBbIT29HuoS evils@valix"
    ];
  };

  environment.variables = {
    LIBGL_ALWAYS_SOFTWARE = "1"; # otherwise some things (f3d, variable layer height in prusaslicer) crash
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

}
